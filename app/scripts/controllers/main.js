'use strict';

/**
 * @ngdoc function
 * @name taskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the taskApp
 */
angular.module('taskApp')
  .controller('MainCtrl', function ($scope, Notification) {
    $scope.data=[
       
        {
            "id" : "1000",
            "name" : "Suresh",
            "surname" : "P",
            "email" :"suresh@umm.digital",
            "amount":"ZZ1000",
            "status":"Active",
            "view":"view",
        },
        {
            "id" : "1001",
            "name" : "Kumar",
            "surname" : "P",
            "email" :"kumar@umm.digital",
            "amount":"ZZ1000",
            "status":"Active",
            "view":"view",
        },
        {
            "id" : "1002",
            "name" : "Cyril",
            "surname" : "S",
            "email" :"cyril@umm.digital",
            "amount":"ZZ2000",
            "status":"Inactive",
            "view":"view",
        },
        {
            "id" : "1003",
            "name" : "Gowri",
            "surname" : "N",
            "email" :"gowri@umm.digital",
            "amount":"ZZ3000",
            "status":"Active",
            "view":"view",
        },
        {
            "id" : "1004",
            "name" : "Murali",
            "surname" : "M",
            "email" :"murali@umm.digital",
            "amount":"ZZ4000",
            "status":"Inactive",
            "view":"view",
        },
        {
            "id" : "1005",
            "name" : "Mari",
            "surname" : "A",
            "email" :"mari@umm.digital",
            "amount":"ZZ5000",
            "status":"Active",
            "view":"view",
        }
  ]
    $scope.viewdiv = "view";
    $scope.editsec = "none";
    $scope.editbox=false;
    $scope.editbox_enable=false;
    $scope.updatesec=false;
    $scope.addform=false;
    $scope.listindex="";
    $scope.new={};
    $scope.view_Div = function(values,index){
        $scope.editbox_enable=false;
        $scope.updatesec=true;
        $scope.listindex=index;
        $scope.name=values.name;
        $scope.email=values.email;
        $scope.status=values.status;
        $scope.amount=values.amount;
        if ($scope.viewdiv == "view"){
             $scope.viewdiv = "viewsec";
             $scope.editbox=true;
        }
        if ($scope.editsec == "none"){
            $scope.editsec = "editsec";
            $scope.editbox=true;
       }
    };
    $scope.changevalue = function(values){
        $scope.addform=false;
        $scope.name=values.name;
        $scope.email=values.email;
        $scope.status=values.status;
        $scope.amount=values.amount;
       
    };
    


  $scope.editvalue=function(){
    $scope.editbox_enable=true;
  }
  $scope.savedetails=function(){
      $scope.viewdiv = "view";
      $scope.editsec = "none";
      $scope.data[$scope.listindex].name=$scope.name;
      $scope.data[$scope.listindex].email=$scope.email;
      $scope.data[$scope.listindex].status=$scope.status;
      $scope.data[$scope.listindex].amount=$scope.amount;
      Notification.success('Details updated Sucessfully');
  }
  $scope.addnew=function(){
    $scope.editbox_enable=false;
    $scope.updatesec=true;
    if ($scope.viewdiv == "view"){
         $scope.viewdiv = "viewsec";
    }
    if ($scope.editsec == "none"){
        $scope.editsec = "editsec";
   }

    $scope.addform=true;
    $scope.updatesec=false;

  }
  
  $scope.adddetails=function(values){
   
    $scope.data.push(values);
     Notification.success('New data created Sucessfully');
    $scope.new={};
    // console.log($scope.new);
   // $scope.new.name="";
  }

  $scope.delete=function(values,index){
   // console.log(values);
    $scope.data.splice(index, 1);
     $scope.viewdiv = "view";
      $scope.editsec = "none";
    Notification.error('Records Deleted Sucessfully');

  }
  
   
  });
