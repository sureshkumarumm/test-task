'use strict';

/**
 * @ngdoc overview
 * @name taskApp
 * @description
 * # taskApp
 *
 * Main module of the application.
 */
angular
  .module('taskApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.utils',
    'ui-notification'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $routeProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/home');

     $stateProvider

     .state('home', {
          url: '/home',
          controller: 'MainCtrl',
          templateUrl: 'views/main.html'
      })
      .state('about', {
          url: '/about',
          controller: 'AboutCtrl',
          templateUrl: 'views/about.html'
      });
    // $routeProvider
    //   .when('/', {
    //     url: '/home',
    //     templateUrl: 'views/main.html',
    //     controller: 'MainCtrl',
    //     controllerAs: 'main'
    //   })
    //   .otherwise({
    //     redirectTo: '/'
    //   });
  })

  .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 3000,
            startTop: 0,
            startRight: 8,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
    });
